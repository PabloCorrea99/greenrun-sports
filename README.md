# GreenRun - Sports

Project Backend Test that is an API REST, which will be used for a sportsbook application. 

## Description

This project is an assessment for the GreenRun company Backend Developer position.
Author: Pablo Correa Morales
NOTE: I wnat to thank GreenRun for the opportunity, with this project I really learned a lot and improved my, no so great, nodejs expertise. 

## Solution Design

### Architecture

For this project I implemented a basic Client-Server Architecture, the main highlight its that for terms of replication and scalability I used Docker.


![Architecture](https://i.ibb.co/x1FhTtf/architecture-drawio.png)


### Data Model

The Data Model is a simple Transactional model with 4 tables. From the propposed architecture I just add the error_logs table in order to record those exceptions that could occur in the system.


![Data Model](https://i.ibb.co/NtM1QHN/Data-Model-Green-Run.png)

### Important Flows

| endpoint | method | Data | function |
| ------ | ------ | ------ | ------ |
| /auth/token | POST | request: JWT Token | With this endpoint we generate the authentication in the system |
| /user/create | POST | request: Dictionary of UserData | Allow admins to create new User role users |
| /user/transactions/* | GET/POST | request: Dictionary of UserData, Dictionary of Transaction Data | Main endpoints of the system, in charge of all the transactions, thats we we implemented in their services the knex transactions module, is critical to keep that Data reliable. Type of transactions: Bet, Winning, Deposit, Withdraw |
| /user/transactions/balance | GET | request: Current User Id | Return the current User balance, calculated in base of the User transactions |
| /admin/bets/event/eventId/results | PUT | request: Bets Result for an specific event | Critical flow, it triggers the winning transactions when update the result of the event |
| /admin/userId/transactions/balance | GET | request: Sepecific User Balance | Return the Sepecific User balance, calculated in base of the User transactions |
| /admin/block-user/userId | PUT | request: specific User Id | It blocks the specific user from the system |
| /admin/update-user/userId | PUT | request: specific User Id | It update the specific data |
| /admin/bets/list | GET | request: Optional a url query with the sport and/or event| Return the list of bets for the url query or all of the system bets |
| /admin/bets/betId/status | PUT | request: specific Bet Id | Changes a specific Bet status (cant change to settled) |


## Testing the Project

You can test the project via two options: Running the postman collection with the AWS server or cloning the repo in your local machine.


### 1. Running PostMan collection in the AWS Server

The easiest way to test the api is via this [PostMan](https://documenter.getpostman.com/view/20418924/Uzs8UNac) collection where the server URL that is pointing to the aws server (http://ec2-23-23-9-159.compute-1.amazonaws.com:5000) in that documentation its the explanaiton on how every endpoint works.

### 2. Running the project in your local machine


- In order to run the porject in your local machine you have to install the needed packages. 
`npm install`
- Then we have to create the Database (greenrun) and apply the migrations via `knex` command `knex migrate:latest` but for better use I implemented the command `npm run migrate` (Keep in mind that it takes the latest file, so you have to remove the all the files and do it one by one in older/newer order). 
- Then run the command `npm run seed`, so you can have some data in your DataBase.
NOTE: You have to set an `.env` file with the following fields: 
````
DB_HOST=
DB_PORT=
DB_USER=
DB_PASSWORD=
DB_NAME=
SERVER_PORT=
TOKEN_SECRET=
````
- Finally you just have to run the `npm start` command and change the server url of the PostMan variables to `localhost:5000` and you are ready to test it in Local
