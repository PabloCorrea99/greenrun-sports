
exports.up = function(knex) {
    return knex.schema
      .createTable('users', function (table) {
        table.increments('id');
        table.string('role', 20).notNullable();
        table.string('first_name', 255).notNullable();
        table.string('last_name', 255);
        table.string('phone', 12).notNullable();
        table.string('email', 255).unique().notNullable();
        table.string('username', 255).notNullable();
        table.string('address', 255);
        table.enu('gender', ['male', 'female', 'other']).nullable();
        table.date('birthdate');
        table.string('country_id', 3).notNullable();
        table.string('city', 255);
        table.string('document_id', 12).notNullable();
        table.enu('user_state', ['active', 'blocked']).defaultTo('active');
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
        table.boolean('deleted').defaultTo(false);
        table.timestamp('deleted_at').nullable();
      });
  };
  
exports.down = function(knex) {
  return knex.schema
    .dropTable('users');
};

