exports.up = function(knex) {
    return knex.schema
      .createTable('bets', function (table) {
        table.increments('id');
        table.integer('bet_option').notNullable();
        table.string('sport', 255).notNullable();
        table.enu('status', ['active', 'cancelled', 'settled']).notNullable();
        table.string('name', 255).notNullable();
        table.string('event_id', 30).notNullable();
        table.float('odd', 5, 2).notNullable();
        table.enu('result', ['won', 'lost']).nullable();
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
        table.boolean('deleted').defaultTo(false);
        table.timestamp('deleted_at').nullable();
      });
  };
  
exports.down = function(knex) {
  return knex.schema
    .dropTable('bets');
};
