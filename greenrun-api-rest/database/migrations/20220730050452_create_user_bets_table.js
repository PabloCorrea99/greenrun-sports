exports.up = function(knex) {
    return knex.schema
      .createTable('user_bets', function (table) {
        table.increments('id');
        table.integer('user_id').unsigned();
        table.integer('bet_id').unsigned();
        table.foreign('user_id').references('users.id');
        table.foreign('bet_id').references('bets.id');
        table.float('odd', 5, 2).notNullable();
        table.float('amount', 10, 2).notNullable();
        table.enu('state', ['open', 'won', 'lost']).notNullable();
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
        table.boolean('deleted').defaultTo(false);
        table.timestamp('deleted_at').nullable();
      });
  };
  
exports.down = function(knex) {
  return knex.schema
    .dropTable('user_bets');
};

