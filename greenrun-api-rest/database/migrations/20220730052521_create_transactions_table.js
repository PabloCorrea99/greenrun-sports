
exports.up = function(knex) {
    return knex.schema
      .createTable('transactions', function (table) {
        table.increments('id');
        table.integer('user_id').unsigned();
        table.foreign('user_id').references('users.id');
        table.float('amount', 10, 2).notNullable();
        table.enu('category', ['deposit', 'withdraw', 'bet', 'winning']).notNullable();
        table.string('status', 20).notNullable();
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
        table.boolean('deleted').defaultTo(false);
        table.timestamp('deleted_at').nullable();
        table.integer('user_bet_id').unsigned();
        table.foreign('user_bet_id').references('user_bets.id');
      });
  };
  
exports.down = function(knex) {
  return knex.schema
    .dropTable('transactions');
};
