exports.up = function(knex) {
    return knex.schema
      .createTable('error_logs', function (table) {
        table.increments('id');
        table.text('traceback').notNullable();
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
      });
  };
  
exports.down = function(knex) {
  return knex.schema
    .dropTable('error_logs');
};
