
exports.seed = function (knex) {
  return knex('bets')
    .del()
    .then(function () {
      return knex('bets').insert([
        {
          bet_option: 1,
          sport: 'soccer',
          status: 'active',
          name: 'Borussia Dormuntd',
          event_id: 'match:000001',
          odd: 1.5,
          result: null
        },
        {
          bet_option: 2,
          sport: 'soccer',
          status: 'active',
          name: 'draw',
          event_id: 'match:000001',
          odd: 2,
          result: null
        },
        {
          bet_option: 3,
          sport: 'soccer',
          status: 'active',
          name: 'Bayern Munich',
          event_id: 'match:000001',
          odd: 3,
          result: null
        },
        {
          bet_option: 1,
          sport: 'soccer',
          status: 'active',
          name: 'Nacional',
          event_id: 'match:000002',
          odd: 2,
          result: null
        },
        {
          bet_option: 2,
          sport: 'soccer',
          status: 'active',
          name: 'draw',
          event_id: 'match:000002',
          odd: 1.5,
          result: null
        },
        {
          bet_option: 3,
          sport: 'soccer',
          status: 'active',
          name: 'Medellin',
          event_id: 'match:000002',
          odd: 14,
          result: null
        },
        {
          bet_option: 1,
          sport: 'basketball',
          status: 'active',
          name: 'Yankees',
          event_id: 'match:000003',
          odd: 2,
          result: null
        },
        {
          bet_option: 2,
          sport: 'basketball',
          status: 'active',
          name: 'Lakers',
          event_id: 'match:000003',
          odd: 1.7,
          result: null
        },
        {
          bet_option: 1,
          sport: 'basketball',
          status: 'active',
          name: 'Celtics',
          event_id: 'match:000004',
          odd: 2,
          result: null
        },
        {
          bet_option: 2,
          sport: 'basketball',
          status: 'active',
          name: 'Miami',
          event_id: 'match:000004',
          odd: 1.4,
          result: null
        },
        {
          bet_option: 1,
          sport: 'football',
          status: 'active',
          name: 'Eagles',
          event_id: 'match:000005',
          odd: 1.2,
          result: null
        },
        {
          bet_option: 2,
          sport: 'football',
          status: 'active',
          name: 'Packers',
          event_id: 'match:000005',
          odd: 5,
          result: null
        },
      ]);
    });
};
