
exports.seed = function (knex) {
  return knex('users')
    .del()
    .then(function () {
      return knex('users').insert([
        {
          role: 'Admin',
          first_name: 'Hettie',
          last_name: 'Marshall',
          phone: '123456789',
          email: 'lantunde@acbo.va',
          username: 'Hettie123',
          address: 'Calle 123 # 10',
          gender: 'female',
          birthdate: '1999-09-24',
          country_id: 'COL',
          city: 'Medellin',
          document_id: '123456789',
        },
        {
          role: 'Admin',
          first_name: 'Pablo',
          last_name: 'Correa',
          phone: '3206946987',
          email: 'correa.pablo1999@gmail.com',
          username: 'Pablo123',
          address: 'Calle 123 # 10',
          gender: 'male',
          birthdate: '1999-09-24',
          country_id: 'COL',
          city: 'Medellin',
          document_id: '123456789',
        },
        {
          role: 'User',
          first_name: 'Felipe',
          last_name: 'Ortega',
          phone: '987456123',
          email: 'felipe@greenrun.com',
          username: 'Felipe123',
          address: 'Calle 123 # 10',
          gender: 'male',
          birthdate: '1999-09-24',
          country_id: 'COL',
          city: 'Medellin',
          document_id: '123456789',
        },
        {
          role: 'User',
          first_name: 'Iara',
          last_name: 'Oliviera',
          phone: '987456123',
          email: 'iara@greenrun.com',
          username: 'Iara123',
          address: 'Calle 123 # 10',
          gender: 'female',
          birthdate: '1999-09-24',
          country_id: 'BRA',
          city: 'Brasilia',
          document_id: '123456789',
        },
      ]);
    });
};
