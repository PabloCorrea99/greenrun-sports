const db = require('../config/db');

module.exports = class Logger {
    async errorLogger(err){
        try {
            await db('error_logs').insert({traceback: err.message}, ['id'])
        } catch (err) {
            throw new Error(err);
        }
    }
}
