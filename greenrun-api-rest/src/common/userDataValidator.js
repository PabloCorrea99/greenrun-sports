
const User = require('../models/user');

module.exports = class UserDataValidator{
    static validate(method, rawData){
        switch (method) {
            case 'create':
                return UserDataValidator.#validateCreation(rawData);       
            case 'update':
                return UserDataValidator.#validateUpdate();
            default:
                return;
        }
    }

    static #validateCreation(rawData){
        const userModel = new User();
        let isValidData = userModel.requiredCreationData.validate(rawData);
        return isValidData;
    }

    static #validateUpdate(){
        return true;
    }
};