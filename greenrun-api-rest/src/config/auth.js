const path = require('path');
require('dotenv').config({ path: path.resolve(__dirname, '../../.env') });
const jwt = require('jsonwebtoken');

function generateAccessToken(email) {
    return jwt.sign(email, process.env.TOKEN_SECRET, { expiresIn: '3600s' });
}

function verifyToken(token){
    const userVerifated = jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
        if (err) return 403
        else return user
    })
    return userVerifated;
}

module.exports = {
    generateAccessToken,
    verifyToken
}

