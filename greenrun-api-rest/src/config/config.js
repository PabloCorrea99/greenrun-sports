const path = require('path');
require('dotenv').config({ path: path.resolve(__dirname, '../../.env') });
const Logger = require('../common/logs');
const AuthenticationService = require('../services/authentication.service');
module.exports = class Config {

    static initializeGlobals() {
        global.PORT = process.env.SERVER_PORT || 3000;
        global.NOT_OBJECT_FOUND = 404;
        global.BAD_REQUEST = 400;
        global.FORBIDDEN_ACCESS_MESSAGE = 'You dont have permissions to perform that action';
        global.errorLogger = new Logger().errorLogger;
        global.isAdmin = new AuthenticationService().isUserAdmin;
    }

}