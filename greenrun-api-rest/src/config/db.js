const knexConfig = require('../../database/knexfile');
const db = require('knex')(knexConfig['development']);

module.exports = db;