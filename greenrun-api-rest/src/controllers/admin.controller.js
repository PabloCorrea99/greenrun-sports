
const AdminService = require('../services/admin.service');
const UserService = require('../services/user.service');

const blockUser = async (req, res, next) => {
    try {
      if(await isAdmin(req.user)){
        const userId = req.params.userId;
        AdminService.blockUser(userId)
          .then(blockedUser => {
              if (blockedUser !== 1){
                  res.status(404).send(`User with Id: ${userId} doesnt exist`);
                } else{
                  res.json(`User with Id: ${userId} has been blocked`);
                } 
          });
      } else {
        res.status(403).send(`You dont have permissions to perform that action`);
      }
    } catch (err) {
      errorLogger(err);
      next(err);
    }
};

const updateUser = async (req, res, next) => {
  try {
    if(await isAdmin(req.user)){
      const userId = req.params.userId;
      const userData = req.body
      UserService.updateUser(userId, userData)
        .then(blockedUser => {
            if (blockedUser !== 1){
                res.status(404).send(`User with Id: ${userId} doesnt exist`);
              } else{
                res.json(`User with Id: ${userId} has been updated`);
              } 
        });
    } else {
      res.status(403).send(`You dont have permissions to perform that action`);
    }
  } catch (err) {
    errorLogger(err);
    next(err);
  }
};

module.exports = {
    blockUser,
    updateUser
};