const e = require('express');
const {generateAccessToken, verifyToken} = require('../config/auth');
const UserService = require('../services/user.service');
const AuthenticationService = require('../services/authentication.service');

const createToken = (req, res, next) => {
    try {
        const userData = UserService.verifyUser({email: req.body.email})
        if (!userData) {
            res.sendStatus(404)
        } else {
            const token = generateAccessToken({ email: req.body.email });
            res.json({token:token, user: req.body.email});
        }
    } catch (err) {
        errorLogger(err);
        next(err);
    }

};

const authenticateToken = (req, res, next) => {
    try{
        const token = req.headers.idtoken
  
        if (token == null) return res.sendStatus(401)
        const tokenResponse = verifyToken(token);
        if (tokenResponse === 403) return res.sendStatus(403);
        AuthenticationService.setUserPermissions(tokenResponse)
        .then(userInfo => {
            req.user = userInfo;
            next()
        })
        .catch(err => res.sendStatus(400))
    } catch(err){
        errorLogger(err);
        res.sendStatus(400);
    }
  }

module.exports = {
    authenticateToken,
    createToken
}