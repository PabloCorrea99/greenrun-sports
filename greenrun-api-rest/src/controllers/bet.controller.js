
const BetService = require('../services/bet.service');

const listBets = async (req, res, next) => {
    try {
        if(await isAdmin(req.user)){
            const eventId = req.query.eventId ? req.query.eventId : '';
            const sport = req.query.sport ? req.query.sport : '';  
            const betsInfo = await BetService.listBets(eventId, sport);
            if (betsInfo.length === 0){
              res.status(404).send(`it seems theres no Bets with those specifications`);
            } else{
              res.json(betsInfo);
            }
        } else {
            res.status(403).send(FORBIDDEN_ACCESS_MESSAGE);
        }
    } catch (err) {
      errorLogger(err);
      next(err);
    }
};

const updateBetStatus = async (req, res, next) => {
    try {
        if(await isAdmin(req.user)){
            const betId = req.params.betId;
            const betStatus = req.body.status;
            if(betStatus !== 'settled'){
                const betResponse = await BetService.updateBetStatus(betId, betStatus);
                if (betResponse === NOT_OBJECT_FOUND){
                    res.status(404).send(`Bet with Id: ${betId} doesnt exist`);
                } else if (betResponse === BAD_REQUEST){
                    res.status(400).send(`Either you try to change a settled 
                        Bet or the status: ${betStatus} is not a valid option`);
                } else {
                    res.json(`Bet status with id: ${betId} changed`);
                }
            } else {
                res.status(400).send('Cannot change the bet status to settled via this endpoint');            
            }
        } else {
            res.status(403).send(FORBIDDEN_ACCESS_MESSAGE);
        }
      } catch (err) {
        errorLogger(err);
        next(err);
    }
};

const updateBetsResults = async (req, res, next) => {
    try {
        if(await isAdmin(req.user)){
            const eventId = req.params.eventId;
            const bets = req.body;
            const betsInfo = await BetService.updateBetsResults(eventId, bets);
            if (betsInfo === NOT_OBJECT_FOUND){
                res.status(404).send('One of the bets you entered doesn\'t exists');
            }
            else if (betsInfo === BAD_REQUEST){
                res.status(400).send('Something went wrong when updating the results');
            }
            else {
                req.body = betsInfo;
                next();
            }
        } else {
            res.status(403).send(FORBIDDEN_ACCESS_MESSAGE);
        }
      } catch (err) {
        errorLogger(err);
        next(err);
    }
};

module.exports = {
    listBets,
    updateBetStatus,
    updateBetsResults
};