const TransactionService = require('../services/transaction.service');
const UserBalanceService = require('../services/userBalance.service');
const UserBetService = require('../services/userBet.service');

const listTransactions = async (req, res, next) => {
    try {
        if(await isAdmin(req.user)){
            const userId = req.query.userId ? req.query.userId : '';
            const category = req.query.category ? req.query.category : '';  
            TransactionService.listTransactions(userId, category)
                .then(transactions => {        
                    if (transactions.length === 0){
                        res.status(404).send(`it seems there\'s no Transactions with those specifications`);
                    } else{
                        res.json(transactions);
                    }}
                );
        } else {
            res.status(403).send(FORBIDDEN_ACCESS_MESSAGE);
        }
    } catch (err) {
        errorLogger(err);
        next(err);
    }
};

const getBalance = async (req, res, next) => {
    try {
        let userId = 0;
        if(await isAdmin(req.user)){
            userId = req.params.userId;
        } else {
            userId = req.user.id;
        }
        UserBalanceService.getUserBalance(userId)
        .then((userBalance) => res.json({balance: userBalance}));     
    } catch (err) {
        errorLogger(err);
        next(err);
    }

};

const withdraw = (req, res, next) => {
    try {
        const userId = req.user.id;
        const withdrawInfo = req.body;
        const withdrawTransaction = new TransactionService(
            'withdraw', {user_id:userId}, withdrawInfo);
        const withdrawResponse = withdrawTransaction.createWithdrawTransaction();
        Promise.resolve(withdrawResponse)
            .then((result) => { 
                if(result) res.json({msg: `Successful withdraw of ${withdrawInfo.amount}`})
            } )
            .catch((error) => next(error))

      } catch (err) {
        errorLogger(err);
        next(err);
    }
};

const deposit = (req, res, next) => {
    try {
        const userId = req.user.id;
        const depositInfo = req.body;
        const depositTransaction = new TransactionService(
            'deposit', {user_id:userId}, depositInfo);
        const depositResponse = depositTransaction.createDepositTransaction();
        Promise.resolve(depositResponse)
            .then((result) => {
                if(result)res.json({msg: `Successful deposit of ${depositInfo.amount}`})
            })
            .catch((error) => next(error))

      } catch (err) {
        errorLogger(err);
        next(err);
    }
};

const bet = (req, res, next) => {
    try {
        const userId = req.user.id;
        const betInfo = req.body;
        const betTransactionInfo = [];
        for (let index = 0; index < betInfo.length; index++) {
            const singleTransaction = new TransactionService(
                'bet', {user_id:userId}, betInfo[index]);
            const singleTransactionResult = singleTransaction.createBetTransaction();
            betTransactionInfo.push(singleTransactionResult);
        }
        Promise.all(betTransactionInfo)
            .then((result) => {
                if ((result.filter(r => r !== true)).length > 0){
                    throw new Error(`Status: ${BAD_REQUEST}`)
                }else{
                    const response = TransactionService.parseBetResponse(betInfo);
                    res.json(response);
                }    
            })
            .catch((error) => next(error))
      } catch (err) {
        errorLogger(err);
        next(err);
    }
};

const winning = async (req, res, next) => {
    try {
        const betInfo = req.body;
        const winningTransactionInfo = [];
        const winningBet = betInfo.find(bet => bet.result === 'won');
        const winningUsersBets = await UserBetService.listWinningUsers(winningBet);
        if(winningUsersBets.length > 0){
            for (let index = 0; index < winningUsersBets.length; index++) {
                const singleTransaction = new TransactionService(
                    'winning', winningUsersBets[index], winningBet);
                const singleTransactionResult = singleTransaction.createWinningTransaction();
                winningTransactionInfo.push(singleTransactionResult);
            }
            Promise.all(winningTransactionInfo)
                .then((result) => res.json(result))
                .catch((error) => next(error))
        } else {
            res.json({msg: 'Any user won this event bets!'})
        }

      } catch (err) {
        errorLogger(err);
        next(err);
    }
};

const listSpecificUserTransactions = (req, res, next) => {
    try {
        const userId = req.user.id;
        const category = req.query.type ? req.query.type : '';  
        TransactionService.listTransactions(userId, category)
            .then(transactions => {        
                if (transactions.length === 0){
                    res.status(404).send(`it seems there\'s no Transactions with those specifications`);
                } else{
                    res.json(transactions);
                }}
            );
    } catch (err) {
        errorLogger(err);
        next(err);
    }
};

module.exports = {
    listTransactions,
    getBalance,
    withdraw,
    deposit,
    bet,
    winning,
    listSpecificUserTransactions
};