
const UserService = require('../services/user.service');
const UserDataValidator = require('../common/userDataValidator');

const getUser = async (req, res, next) => {
    try {
      const userId = req.user.id;
      const userInfo = await UserService.getUser(userId);
      if (!userInfo){
        res.status(404).send(`User with Id: ${userId} doesnt exist`);
      } else{
        res.json(userInfo);
      } 
    } catch (err) {
      errorLogger(err);
      next(err);
    }
};

const createUser = async (req, res, next) => {
    try {
        const userData = req.body;
        const isCleanUserData = UserDataValidator.validate('create', userData[0]);
        if (isCleanUserData){
            const userInfo = await UserService.createUser(userData[0]);
            if (!userInfo){
                res.status(400).send(`User with Id: ${userInfo} already exists`);
            } else {
                res.json(userInfo);
            }
        } else {
            res.status(400).send('There was a data error in your request'); 
        }
      } catch (err) {
        next(err);
    }
};

const updateUser = async (req, res, next) => {
    try {
        const userId = req.user.id;
        const userData = req.body;
        const isCleanUserData = UserDataValidator.validate('update', userData[0]);
        if (isCleanUserData){
            const userInfo = await UserService.updateUser(userId, userData[0]);
            if (!userInfo){
                res.status(404).send(`User with Id: ${userId} doesnt exist`);
            } else {
                res.json(userInfo);
            } 
        } else {
            res.status(400).send('There was a data error in your request'); 
        }
      } catch (err) {
        errorLogger(err);
        next(err);
    }
};

module.exports = {
    getUser,
    updateUser,
    createUser
};