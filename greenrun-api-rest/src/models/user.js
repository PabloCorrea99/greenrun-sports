const datamodel = require('data-model');

module.exports = class User{
    constructor(){
        this.requiredCreationData = datamodel('user');
        this.requiredCreationData.setTemplate({
            role: String,
            first_name: String,
            last_name: String,
            phone: String,
            email: String,
            username: String,
            address: String,
            gender: String,
            birthdate: String,
            country_id: String,
            city: String,
            document_id: String
        });
    }
}