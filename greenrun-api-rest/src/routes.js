
const express = require ('express');
const app = express();

const user = require(`./routes/user.route`);
const admin = require(`./routes/admin.route`);

app.use('/user', user);
app.use('/admin', admin);

module.exports = app;
