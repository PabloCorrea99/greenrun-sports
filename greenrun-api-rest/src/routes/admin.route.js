const express = require('express');
const router = express.Router();
const authController = require('../controllers/authentication.controller');
const betController = require('../controllers/bet.controller');
const adminController = require('../controllers/admin.controller');
const transactionController = require('../controllers/transaction.controller');


//bets endpoints
router.get('/bets/list',
    authController.authenticateToken,
    betController.listBets
)
router.put('/bets/:betId/status',
    authController.authenticateToken,
    betController.updateBetStatus
)
router.put('/bets/event/:eventId/results',
    authController.authenticateToken,
    betController.updateBetsResults,
    transactionController.winning
)

//user admin-management endpoints
router.put('/block-user/:userId',
    authController.authenticateToken,
    adminController.blockUser
)
router.put('/update-user/:userId',
    authController.authenticateToken,
    adminController.updateUser
)

//admin transaction endpoints
router.get('/transactions/list',
    authController.authenticateToken,
    transactionController.listTransactions
)
router.get('/:userId/transactions/balance',
    authController.authenticateToken,
    transactionController.getBalance
)

module.exports = router;