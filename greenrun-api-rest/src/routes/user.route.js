
const express = require('express');
const app = express();
const router = express.Router();
const authController = require('../controllers/authentication.controller');
const userController = require('../controllers/user.controller');
const transactionController = require('../controllers/transaction.controller');

//user basic CRUD endpoints
router.post('/auth/token', authController.createToken)
router.get('/',
    authController.authenticateToken,
    userController.getUser
)
router.put('/update',
    authController.authenticateToken, 
    userController.updateUser
)
router.post('/create',
    userController.createUser
)

//user transactions endpoints
router.get('/transactions/list',
    authController.authenticateToken,
    transactionController.listSpecificUserTransactions
)
router.get('/transactions/balance',
    authController.authenticateToken,
    transactionController.getBalance
)
router.post('/transactions/withdraw',
    authController.authenticateToken,
    transactionController.withdraw
)
router.post('/transactions/deposit',
    authController.authenticateToken,
    transactionController.deposit
)
router.post('/transactions/bet',
    authController.authenticateToken,
    transactionController.bet
)

module.exports = router;