
const Config = require('./config/config');
Config.initializeGlobals();
const express = require('express');
const db = require('./config/db');
const routes = require('./routes');
const app = express();


app.use(express.json());
app.use('/api', routes);

app.listen(
    PORT, 
    () => console.log(`Listening on port ${PORT}`)
);
