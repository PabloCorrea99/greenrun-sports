const db = require('../config/db');
const UserService = require('./user.service');

class AdminService {

  static async blockUser(userId) {
      try {    
        const user = await UserService.verifyUser({ id: userId });
        if (!user){
            throw new Error(`Status: ${NOT_OBJECT_FOUND}`);
        } 
        else if(user.role === 'Admin' || user.user_state === 'active') {
            throw new Error(`Status: ${BAD_REQUEST}`);
        } 
        const updatedUser = await db('users')
            .where({ id: userId })
            .update({user_state: 'blocked'}, ['id']);
        return updatedUser;
      } catch (err) {
        errorLogger(err);
        return err
      }

  }
}

module.exports = AdminService;