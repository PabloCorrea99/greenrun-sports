const db = require('../config/db');
const UserService = require('./user.service');

class AuthenticationService {

  async isUserAdmin(userData){
    try {    
        const user = await UserService.verifyUser({ email: userData.email });
        if (!user){
            throw new Error(`Status: ${NOT_OBJECT_FOUND}`);
        }
        if(user.role === 'Admin') return true;
        else return false;
    } catch(err){
        errorLogger(err)
        throw new Error(`Status: ${BAD_REQUEST}`);
    }
  }

  static async setUserPermissions(userData) {
    try {    
        const user = await UserService.verifyUser({ email: userData.email });
        if (!user){
            throw new Error(`Status: ${NOT_OBJECT_FOUND}`);
        }
        userData.role = user.role;
        userData.id = user.id;
        return userData;
    } catch(err){
        errorLogger(err)
        throw new Error(`Status: ${BAD_REQUEST}`);
    }
  }
}

module.exports = AuthenticationService;