const db = require('../config/db');


class BetService {
    static async listBets(eventId, sport) {

        const query = BetService.#parseListBetsQuery(eventId.length, sport.length)
        const betsList = await db.raw(query, {eventId, sport});
        return betsList[0];
    }

    static async updateBetStatus(betId, betStatus){
        try {
            const betData = await BetService.verifyBet({id: betId});
            if (!betData) throw new Error(`Status: ${NOT_OBJECT_FOUND}`);
            if (betData.status === 'settled' || betStatus === 'settled') {
                throw new Error(`Status: ${BAD_REQUEST}`);
            }
            const updatedBet = await db('bets')
            .where({ id: betId })
            .update({status: betStatus}, ['id']);
            return updatedBet;
        } catch (err) {
            errorLogger(err);
            return err;
        }  
    }

    static async updateBetsResults(eventId, betsResults){
        try {
            await BetService.verifyEvent({event_id: eventId});
            const updatedBets = await Promise.all(betsResults.map((bet) => {
                return BetService.#updateSpecificBetResult(bet, eventId);
            }));
            return updatedBets;
        } catch (err) {
            errorLogger(err);
            return err;
        }

    }

    static async verifyBet(betData) {
        const bet = await db('bets').first('*').where(betData);
        return bet
    }

    static async verifyEvent(eventData) {
        const event = await db('bets').select('*').where(eventData);
        return event
    }

    static async #updateSpecificBetResult(betData, eventId) {
        try {
            const betInfo = await BetService.verifyBet(
                {bet_option: betData.betOption, event_id: eventId}
            );
            if (!betInfo) {
                throw new Error(`Status: ${NOT_OBJECT_FOUND}`);
            }
            else if (betInfo.status === 'settled' 
                || betInfo.status === 'cancelled'){
                throw new Error(`Status: ${BAD_REQUEST}`);
            }
            const query = await db('bets')
            .where({id: betInfo.id, event_id: eventId})
            .update({result: betData.result, status: 'settled'}, ['id']);
            if (query === 1) {
                const newBetInfo = await BetService.verifyBet({id: betInfo.id});
                return newBetInfo;
            } else {
                throw new Error(`Status: ${BAD_REQUEST}`);
            }
        } catch (err) {
            errorLogger(err);
            return err;
        }

    }

    static #parseListBetsQuery(eventIdLength, sportLength){
        let query = 'select * from bets';
        let queryOperator = '';
        let eventIdWhereValue = '';
        let sportWhereValue = '';
        let whereValue = '';

        if(eventIdLength > 0) {
            eventIdWhereValue = ' event_id = :eventId';
            whereValue = ' where';
        }
        if(sportLength > 0) {
            sportWhereValue = ' sport = :sport';
            whereValue = ' where';
        }

        if(eventIdLength > 0 && sportLength > 0){
            queryOperator = ' and';
        }

        return query.concat(whereValue, eventIdWhereValue, queryOperator, sportWhereValue);
    }
}

module.exports = BetService;