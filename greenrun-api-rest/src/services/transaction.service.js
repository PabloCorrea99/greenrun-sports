const db = require('../config/db');
const BetService = require('./bet.service');
const UserService = require('./user.service');
const UserBalanceService = require('./userBalance.service');
const UserBetService = require('./userBet.service');
const formatter = new Intl.NumberFormat('en-US', {
    minimumFractionDigits: 2,      
    maximumFractionDigits: 2,
 });

class TransactionService {

    constructor(type, userInfo, transactionInfo){
        this.type = type;
        this.userInfo = userInfo;
        this.transactionInfo = transactionInfo;
        this.transactionDict  = {
            user_id: userInfo.user_id,
            amount: transactionInfo.amount,
            category: type
        };

    }

    static async listTransactions(userId, category) {
        try {
            const query = TransactionService.#parseListTransactionQuery(userId.length, category.length);
            const transactionList = await db.raw(query, {userId, category});
            return transactionList[0];
        } catch (err) {
            errorLogger(err);
            return err;
        }
    }

    async createBetTransaction(){
        try {
            const userData = await UserService.verifyUser({id: this.userInfo.user_id});
            const userHasEnoughMoney = await TransactionService.#checkIfUserHasEnoughMoney(
                userData, this.transactionInfo);
            if(userHasEnoughMoney){
                const eventData = await BetService.verifyEvent({event_id: this.transactionInfo.eventId});
                const betData = await BetService.verifyBet({event_id: this.transactionInfo.eventId,
                    bet_option: this.transactionInfo.betOption});
                if(!betData || !eventData) {
                    throw new Error(`Status: ${NOT_OBJECT_FOUND}`);
                }
                const transactionCompleted = await TransactionService.handleTransaction(
                    'bet', userData, this.transactionInfo, this.transactionDict, betData);
                return transactionCompleted;
            } else {
                throw new Error(`Status: ${BAD_REQUEST}`);
            }   
        } catch (err) {
            errorLogger(err);
            return err;
        }
    }

    async createWinningTransaction(){
        try {
            const userData = await UserService.verifyUser({id: this.userInfo.user_id});
            this.transactionDict.amount = TransactionService.#calculateTotalWinningAmount(
                this.transactionInfo, this.userInfo);
            this.transactionDict.user_bet_id = this.userInfo.id;
            const transactionCompleted = await TransactionService.handleTransaction(
                'winning', userData, this.transactionInfo, this.transactionDict, {});
            if(transactionCompleted)
                return `Winning Transaction for user: ${userData.id} successfully processed!`;
            else
                throw new Error(`Status: ${BAD_REQUEST}`);
        } catch (err) {
            errorLogger(err);
            return err;
        }
    }

    async createDepositTransaction(){
        try {
            const userData = await UserService.verifyUser({id: this.userInfo.user_id});
            if(!userData) {
                throw new Error(`Status: ${NOT_OBJECT_FOUND}`);
            }
            const transactionCompleted = await TransactionService.handleTransaction(
                'deposit', userData, this.transactionInfo, this.transactionDict, {});
            return transactionCompleted;  
        } catch (err) {
            errorLogger(err);
            return err;
        }
    }

    async createWithdrawTransaction(){
        try {
            const userData = await UserService.verifyUser({id: this.userInfo.user_id});
            const userHasEnoughMoney = await TransactionService.#checkIfUserHasEnoughMoney(
                userData, this.transactionInfo);
            if(userHasEnoughMoney){
                const transactionCompleted = await TransactionService.handleTransaction(
                    'withdraw', userData, this.transactionInfo, this.transactionDict, {});
                return transactionCompleted;
            } else {
                throw new Error(`Status: ${BAD_REQUEST}`);
            } 
        } catch (err) {
            errorLogger(err);
            return err;
        }
    }

    static async handleTransaction(type, userInfo, transactionInfo, transactionDict, optionalData){
        const trxProvider = db.transactionProvider();
        const trx = await trxProvider();
        try {
            transactionDict.status = 'pending'; 
            const createdTransactionId = await TransactionService.createTransaction(
                trx, transactionDict);
            if(type === 'bet'){
                const createdUserBetId = await UserBetService.createUserBet(
                    trx, userInfo, optionalData, transactionInfo);
                await TransactionService.updateBetTransaction(trx, createdTransactionId, createdUserBetId);
            } else {
                await TransactionService.updateTransaction(trx, createdTransactionId);
            }
            await trx.commit();
            return trx.isCompleted();
          } catch (err) {
            errorLogger(err);
            trx.rollback();
            return err;
          }
    }

    static async createTransaction(trx, transactionInfo){
        const transactionId = await trx('transactions')
            .insert(transactionInfo, 'id')
        return transactionId[0];
    }

    static async updateBetTransaction(trx, transactionId, userBetId){
        await trx('transactions')
        .where({id: transactionId})
        .update({status: 'approved', user_bet_id: userBetId}, ['id']);
    }

    static async updateTransaction(trx, transactionId){
        await trx('transactions')
        .where({id: transactionId})
        .update({status: 'approved'}, ['id']);
    }

    static parseBetResponse(betInfo){
        const parsedBet = betInfo.map(bet => {
            return {
                status:'approved',
                msg: `Successful bet for Event: ${bet.eventId}, Option: ${bet.betOption}`,
            }
        })
        return parsedBet;
    }

    static #parseListTransactionQuery(userIdLength, categoryLength){
        let query = 'select * from transactions';
        let queryOperator = '';
        let userIdWhereValue = '';
        let categoryWhereValue = '';
        let whereValue = '';

        if(userIdLength > 0) {
            userIdWhereValue = ' user_id = :userId';
            whereValue = ' where';
        }
        if(categoryLength > 0) {
            categoryWhereValue = ' category = :category';
            whereValue = ' where';
        }

        if(userIdLength > 0 && categoryLength > 0){
            queryOperator = ' and';
        }

        return query.concat(whereValue, userIdWhereValue, queryOperator, categoryWhereValue);
    }

    static #calculateTotalWinningAmount(transactionInfo, userBetInfo){
        try {
            if(transactionInfo.odd == userBetInfo.odd){
                const odd = transactionInfo.odd;
                const amount = userBetInfo.amount;
                return parseFloat(formatter.format(odd * amount));
            } else {
                throw new Error(`Status: ${BAD_REQUEST}`);
            }
        } catch (err) {
            errorLogger(err);
            return err;
        }
    }

    static async #checkIfUserHasEnoughMoney(userInfo, transactionInfo){
        try {
            const userBalance = await UserBalanceService.getUserBalance(userInfo.id);
            const substractionResult = userBalance - transactionInfo.amount;
            if( substractionResult >= 0.0) return true;
            else return false;
        } catch (err) {
            return err;
        }
    }
}

module.exports = TransactionService;