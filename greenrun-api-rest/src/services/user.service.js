const db = require('../config/db');

class UserService {
  static async getUser(userId) {
    const userInfo = await UserService.verifyUser({ id: userId });
    return userInfo;
  }

  static async createUser(userData) {
    const newUserId = await db('users')
      .insert(userData, ['id'])
      .onConflict('email')
      .ignore();
    return newUserId[0];
  }

  static async updateUser(userId, userData) {
    try {    
      const user = await UserService.verifyUser({ id: userId });
      if (!user) throw new Error(`Status: ${NOT_OBJECT_FOUND}`);
      if(userData.id || user.role === 'Admin') {
        throw new Error(`Status: ${BAD_REQUEST}`);
      }
      const updatedUser = await db('users')
        .where({ id: userId })
        .update(userData, ['id']);
      return updatedUser;
    } catch(err){
        errorLogger(err)
        throw new Error(`Status: ${BAD_REQUEST}`);
    }
  }

  static async verifyUser(userData) {
    const user = await db('users').first('*').where(userData);
    return user;
  }
}

module.exports = UserService;
