const db = require('../config/db');

class UserBalanceService {

  static async getUserBalance(userId) {
    try {
      const balanceData = await db('transactions')
        .where({ user_id: userId })
        .select('*');
      if(!balanceData) throw new Error(`Status: ${NOT_OBJECT_FOUND}`);

      return UserBalanceService.calculateBalance(balanceData);
    } catch (err) {
      errorLogger(err);
      return err;
    }
  }

  static calculateBalance(balanceInfo){
    let balance = 0.0;
    for (let index = 0; index < balanceInfo.length; index++) {
      if(balanceInfo[index].category === 'bet'
        || balanceInfo[index].category === 'withdraw'){
          balance -= balanceInfo[index].amount;
        } else {
          balance += balanceInfo[index].amount;
        }
    }
    return balance;
  }
}

module.exports = UserBalanceService;