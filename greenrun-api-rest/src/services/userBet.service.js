
const db = require('../config/db');
const BetService = require('./bet.service');



class UserBetService {

    static async createUserBet(trx, userInfo, betInfo, userBetInfo){
        try {
            const eventData = await BetService.verifyEvent({event_id: betInfo.event_id});
            for (let index = 0; index < eventData.length; index++) {
                if(eventData[index].status !== 'active'){
                    throw new Error(`Status: ${BAD_REQUEST}`);
                }
                const userBetData = await UserBetService.#verifyUserBet({
                    user_id: userInfo.id, bet_id: eventData[index].id
                });
                if(userBetData) {
                    throw new Error(`Status: ${BAD_REQUEST}`);
                } 
            }
            const parsedUserBetData = {
                user_id: userInfo.id,
                bet_id: betInfo.id,
                odd: betInfo.odd,
                amount: userBetInfo.amount,
                state: 'open'
            }
            const newUserBetId = await trx('user_bets')
            .insert(parsedUserBetData, ['id'])
            return newUserBetId[0];
        } catch (err) {
            trx.rollback();
            errorLogger(err);
            throw new Error(err);
        }
    }

    static async listWinningUsers(betInfo){
        try {
            const winningUsers = await db('user_bets')
                .select('*').where({bet_id: betInfo.id});
            return winningUsers;
        } catch (err) {
            errorLogger(err);
            throw new Error(err);
        }
    }

    static async #verifyUserBet(betData) {
        const bet = await db('user_bets').first('*').where(betData);
        return bet
    }
}

module.exports = UserBetService;